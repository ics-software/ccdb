/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.client.impl;

import java.io.InputStream;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Response;

import org.openepics.discs.client.CCDBClient;
import org.openepics.discs.conf.jaxb.DeviceTypeXml;
import org.openepics.discs.conf.jaxb.lists.DeviceTypeListXml;
import org.openepics.discs.conf.jaxrs.client.DeviceTypeClient;

import com.google.common.base.Preconditions;

/**
 * This is CCDB service clientdataType parser that is used to get data from server.
 * <p>
 * All class methods are static.
 * </p>
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */

public class DeviceTypeClientImpl implements DeviceTypeClient {

    private static final Logger LOG = Logger.getLogger(DeviceTypeClientImpl.class.getCanonicalName());

    private static final String COULDN_T_RETRIEVE_DATA_FROM_SERVICE_AT = "Couldn't retrieve data from service at ";

    private static final String PATH_DEVICE_TYPES = "deviceTypes";

    @Nonnull private final CCDBClient client;

    public DeviceTypeClientImpl(CCDBClient client) { this.client = client; }

    /**
     * Requests a {@link DeviceTypeListXml} containing a {@link List} of all {@link DeviceTypeXml}s from the REST service.
     *
     * @throws ResponseException if data couldn't be retrieved
     *
     * @return {@link List} of all {@link DeviceTypeXml}s
     */
    @Override
    public List<DeviceTypeXml> getAllDeviceTypes() {
        LOG.fine("Invoking getAllDeviceTypes.");

        final String url = client.buildUrl(PATH_DEVICE_TYPES);
        try (final ClosableResponse response = client.getResponse(url)) {
            return response.readEntity(DeviceTypeListXml.class).getDeviceTypes();
        } catch (Exception e) {
            throw new ResponseException(COULDN_T_RETRIEVE_DATA_FROM_SERVICE_AT + url + ".", e);
        }
    }

    /**
     * Requests particular {@link DeviceTypeXml} from the REST service.
     *
     * @param name the name of the desired DeviceTypeXml
     *
     * @throws ResponseException  if data couldn't be retrieved
     *
     * @return {@link DeviceTypeXml}
     */
    @Override
    public DeviceTypeXml getDeviceType(String name) {
        LOG.fine("Invoking getDeviceType. name=" + name);
        Preconditions.checkNotNull(name);

        final String url = client.buildUrl(PATH_DEVICE_TYPES, name);
        try (final ClosableResponse response = client.getResponse(url)) {
            return response.readEntity(DeviceTypeXml.class);
        } catch (Exception e) {
            throw new ResponseException(COULDN_T_RETRIEVE_DATA_FROM_SERVICE_AT + url + ".", e);
        }
    }

    /**
     * Requests particular file attachment from the REST service. To read the attachment file use
     * {@link Response#readEntity(Class)}: <code>Reponse.readEntity(InputStream.class)</code>.
     *
     * @param name the {@link DeviceTypeXml}
     * @param fileName the name of the attachment
     * @return {@link Response}
     */
    @Override
    public InputStream getAttachment(String name, String fileName) {
        LOG.fine("Invoking getAttachment. name=" + name + ", fileName" + fileName);
        Preconditions.checkNotNull(name);
        Preconditions.checkNotNull(fileName);

        final String url = client.buildUrl(PATH_DEVICE_TYPES, name, "download", fileName);
        try (final ClosableResponse response = client.getResponse(url)) {
            return response.readEntity(InputStream.class);
        } catch (Exception e) {
            throw new ResponseException(COULDN_T_RETRIEVE_DATA_FROM_SERVICE_AT + url + ".", e);
        }
    }
}
