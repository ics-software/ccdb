/*
 * Copyright (c) 2018 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.db;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.sql.DataSource;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.MigrationInfo;

@Singleton
@Startup
@TransactionManagement(value = TransactionManagementType.BEAN)
public class DatabaseMigration {

    private static final Logger LOGGER = Logger.getLogger(DatabaseMigration.class.getName());

    @Resource(lookup = "java:/org.openepics.discs.conf.data")
    private DataSource dataSource;

    @PostConstruct
    private void onStartup() {
        if ("true".equalsIgnoreCase(System.getProperty("flyway.enabled"))) {
            if (dataSource == null) {
                throw new EJBException("DataSource could not be found!");
            }

            Map<String, String> placeholders = new HashMap<>();
            System.getProperties().entrySet().forEach(e -> {
                String key = (String) e.getKey();
                String value = (String) e.getValue();
                if (key.startsWith("flyway.placeholders.")) {
                    placeholders.put(key.substring(20), value);
                }
            });

            Flyway flyway = new Flyway();
            flyway.setPlaceholders(placeholders);
            flyway.setDataSource(dataSource);

            MigrationInfo migrationInfo = flyway.info().current();
            if (migrationInfo == null) {
                LOGGER.log(Level.INFO, "Database is not versioned");
            } else {
                LOGGER.log(
                        Level.INFO,
                        "Database contains data version: " + migrationInfo.getVersion()
                            + " : " + migrationInfo.getDescription());
            }

            flyway.migrate();
            LOGGER.log(Level.INFO, "Database migrated to data version: " + flyway.info().current().getVersion());
        }
    }
}
