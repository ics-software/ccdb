CREATE TABLE slot_relationship_label (
	id int8 NOT NULL,
	"label" varchar(255) NOT NULL,
	CONSTRAINT slot_relationship_label_pk PRIMARY KEY (id),
	CONSTRAINT slot_relationship_label_un UNIQUE ("label")
);
ALTER TABLE slot_pair ADD "label" int8 NULL DEFAULT NULL;
ALTER TABLE slot_pair ADD CONSTRAINT fk_slot_pair_slot_relationship_label FOREIGN KEY ("label") REFERENCES slot_relationship_label(id) ON DELETE SET NULL;
