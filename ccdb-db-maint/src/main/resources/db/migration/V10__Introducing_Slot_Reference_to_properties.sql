ALTER TABLE property ADD is_slot_reference bool NULL;

ALTER TABLE slot_property_value ADD ref_to_slot int8 NULL;
ALTER TABLE slot_property_value ADD CONSTRAINT slot_property_value_fk FOREIGN KEY (ref_to_slot) REFERENCES slot(id) ON DELETE SET NULL;