/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.services.fbs;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openepics.cable.client.impl.ResponseException;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for {@link FbsClient} class (using MockServer, details: {@link FbsServiceMock}).
 *
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 *
 * @see FbsClient
 **/
public class FbsClientTest extends FbsServiceMock {
    private FbsClient client = null;

    @Before
    public void init() {
        client = new FbsClient(FbsServiceMock.TEST_FBS_URL);
    }

    @After
    public void dispose() {
        client = null;
    }

    @Test
    public void getFbsListing_readTagNames() {
        final List<FbsElement> fbsElements = client.getFbsListing();
        List<String> tags = FbsUtil.readTags(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tags);
        assertFalse(fbsElements.isEmpty());
        assertFalse(tags.isEmpty());
    }

    @Test(expected = ResponseException.class)
    public void getFbsListing_readTagNames_fail404() {
        clean();
        createExpectationFail(Collections.emptyList());
        client.getFbsListing();
    }

    @Test
    public void getFbsTagsForEssName() {
        final String essName = "HBL-110RFC:RFS-ADR-11002";
        final List<FbsElement> fbsElements = client.getFbsListingForEssName(essName);

        assertNotNull(fbsElements);
        assertEquals(fbsElements.size(), 1);
        assertEquals(fbsElements.get(0).essName, "HBL-110RFC:RFS-ADR-11002");
        assertEquals(fbsElements.get(0).tag, "=ESS.ACC.A05.A12.E01.K01.K01.KF01.K02");
    }

    @Test
    public void getFbsTagsForEssName_noMatch() {
        final String essName = "noMatch";
        final List<FbsElement> fbsElements = client.getFbsListingForEssName(essName);

        assertNotNull(fbsElements);
        assertEquals(fbsElements.size(), 0);
    }

    @Test
    public void getFbsTagsForEssName_ambiguous() {
        final String essName = "MBL-010RFC:RFS-SIM-410";
        final List<FbsElement> fbsElements = client.getFbsListingForEssName(essName);

        assertNotNull(fbsElements);
        assertEquals(fbsElements.size(), 2);
        assertEquals(fbsElements.get(0).essName, "MBL-010RFC:RFS-SIM-410");
        assertEquals(fbsElements.get(0).tag, "=ESS.ACC.A04.A02.E04.F01.F02.KF01");
        assertEquals(fbsElements.get(1).essName, "MBL-010RFC:RFS-SIM-410");
        assertEquals(fbsElements.get(1).tag, "=ESS.ACC.A04.A02.E04.F01.F02.KF02");
    }

    @Test
    public void getFbsListingForTagQuery() {
        final String tag = "=ESS.ACC.A05.A19.E03";
        String tagQuery = "%" + tag + "%";
        final int max_results = 5;
        final List<FbsElement> fbsElements = client.getFbsListingForTagQuery(tagQuery, max_results);
        List<String> tags = FbsUtil.readTags(fbsElements);

        assertNotNull(fbsElements);
        assertEquals(fbsElements.size(), max_results);

        for (String value : tags) {
            assertNotNull(value);
            assertTrue(value.contains(tag));
        }
    }

    @Test
    public void getFbsListingForTagQuery_noMatch() {
        final String tag = "noMatch";
        String tagQuery = "%" + tag + "%";
        final int max_results = 5;
        final List<FbsElement> fbsElements = client.getFbsListingForTagQuery(tagQuery, max_results);
        List<String> tags = FbsUtil.readTags(fbsElements);

        assertNotNull(fbsElements);
        assertEquals(fbsElements.size(), 0);
        assertNotNull(tags);
        assertEquals(tags.size(), 0);
    }

    @Test
    public void getFbsListingForTagQuery_invalidMaxResults() {
        final String tag = "=ESS.ACC.A05.A19.E03";
        String tagQuery = "%" + tag + "%";
        final int max_results = -5;
        final List<FbsElement> fbsElements = client.getFbsListingForTagQuery(tagQuery, max_results);
        List<String> tags = FbsUtil.readTags(fbsElements);

        assertNotNull(fbsElements);
        assertEquals(fbsElements.size(), 0);
        assertNotNull(tags);
        assertEquals(tags.size(), 0);
    }
}
