/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.openepics.discs.conf.services.slot;

import javax.ejb.EJBException;

/**
 * An exception that is thrown from {@link SlotRelationshipService} when error occurs. This exception also triggers the
 * rollback of the transactions that was started inside SlotRelationshipService method.
 * 
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class SlotRelationshipServiceException extends EJBException {
    /**
     * @see EJBException#EJBException()
     */
    public SlotRelationshipServiceException() {
    }

    /**
     * @param message
     *            the message
     * @see RuntimeException#RuntimeException(String)
     */
    public SlotRelationshipServiceException(String message) {
        super(message);
    }

    /**
     * @param cause
     *            the cause
     * @see RuntimeException#RuntimeException(Throwable)
     */
    public SlotRelationshipServiceException(Exception cause) {
        super(cause);
    }

    /**
     * @param message
     *            the message
     * @param cause
     *            the cause
     * @see RuntimeException#RuntimeException(String, Throwable)
     */
    public SlotRelationshipServiceException(String message, Exception cause) {
        super(message, cause);
    }
}
