/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.views;

import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import org.apache.commons.lang3.BooleanUtils;
import org.openepics.discs.conf.ent.ComponentType;
import org.openepics.discs.conf.ent.ConfigurationEntity;
import org.openepics.discs.conf.ent.DataType;
import org.openepics.discs.conf.ent.NamedEntity;
import org.openepics.discs.conf.ent.Property;
import org.openepics.discs.conf.ent.PropertyValue;
import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.ent.SlotPropertyValue;
import org.openepics.discs.conf.ent.Unit;
import org.openepics.discs.conf.ent.values.TimestampValue;
import org.openepics.discs.conf.ent.values.Value;
import org.openepics.discs.conf.ui.util.UiUtility;
import org.openepics.discs.conf.util.BuiltInDataType;
import org.openepics.discs.conf.util.Conversion;
import org.openepics.discs.conf.util.ConversionException;
import org.openepics.discs.conf.util.EntityAttributeKind;
import org.openepics.discs.conf.util.PropertyValueUIElement;

import com.google.common.base.CharMatcher;

/**
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 *
 * @param <E> the type of the view parent entity
 */
public class EntityAttrPropertyValueView<E extends ConfigurationEntity & NamedEntity> extends EditableEntityAttributeView<E> {

	/**
	 * CS ENTRY INTEGRATION DISABLED AS CS ENTRY NO LONGER EXISTS.
	 * DISABLE AND / OR RETURN FALSE / EMPTY VALUE(S).
	 */

    private static final Logger LOGGER = Logger.getLogger(EntityAttrPropertyValueView.class.getCanonicalName());

    private static final long serialVersionUID = 1L;

    private static final String INCORRECT_VALUE = "Incorrect value: ";

    private static final String MULTILINE_DELIMITER = "(\\r\\n)|\\r|\\n";

    private PropertyValue propertyValue;
    private boolean propertyNameChangeDisabled;

    /**
     * @param propertyValue the {@link PropertyValue}
     * @param kind the kind of of {@link PropertyValue}
     * @param viewParent the view parent of the {@link PropertyValue} (the one selected in the table)
     * @param propertyValueParent the actual parent of the {@link PropertyValue} (usually {@link ComponentType})
     * @param <P> the type of the of the actual parent
     */
    public <P extends ConfigurationEntity & NamedEntity>
            EntityAttrPropertyValueView(PropertyValue propertyValue, EntityAttributeKind kind, E viewParent,
                                        P propertyValueParent) {
        this(propertyValue, viewParent, propertyValueParent);
        setKind(kind);
    }

    /**
     * @param propertyValue the {@link PropertyValue}
     * @param viewParent the parent of the {@link PropertyValue}
     */
    public EntityAttrPropertyValueView(PropertyValue propertyValue, E viewParent) {
        this(propertyValue, viewParent, null);
    }

    /**
     * @param propertyValue the {@link PropertyValue}
     * @param viewParent the view parent of the {@link PropertyValue} (the one selected in the table)
     * @param propertyValueParent the actual parent of the {@link PropertyValue} (usually {@link ComponentType})
     * @param <P> the type of the of the actual parent
     */
    public <P extends ConfigurationEntity & NamedEntity> EntityAttrPropertyValueView(PropertyValue propertyValue,
            E viewParent, P propertyValueParent) {
        super(viewParent, propertyValueParent != null ? propertyValueParent.getName() : "");
        this.propertyValue = propertyValue;
        setKind(propertyValueParent == null ? getEntityKind(viewParent) : getEntityKind(propertyValueParent));
    }

    @Override
    public String getId() {
        return propertyValue.getId().toString();
    }

    @Override
    public String getName() {
        return propertyValue.getProperty().getFullName();
    }

    /** @return A String representation of the associated entity */
    @Override
    public String getValue() {
        return getPropertyValue();
    }

    /**
     * Sets attribute value using its String representation.
     * See {@link #setPropertyValue(String)}.
     *
     * @param value attribute value
     */
    @Override
    public void setValue(String value) {
        uiValue = value;
        try {
            setPropertyValue(value);
        } catch (ConversionException e) {
            LOGGER.fine("Conversation error at attribute cell edition: " + e.getMessage());
        }
    }

    /**
     * @return <code>true</code> if entity attribute's property is a property alias and it is set or
     * entity attribute's property value is set
     */
    @Override
    public boolean isValueSet() {
        SlotPropertyValue aliasedProperty = getAliasedProperty();
        if (aliasedProperty != null) {
            return true;
        }

        return getValue() != null;
    }

    /**
     * Tells the UI that a property is a slot reference, or not
     *
     * @return
     *       <code>true</code> if property is a slot reference
     *       or <code>false</code> if property is not a slot reference
     */
    public boolean isSlotReference(){
        return propertyValue.getProperty() != null
                && BooleanUtils.toBoolean(propertyValue.getProperty().getSlotReference());
    }

    @Override
    public DataType getType() {
        return propertyValue.getProperty() != null ? propertyValue.getProperty().getDataType() : null;
    }

    @Override
    public Unit getUnit() {
        return propertyValue.getProperty().getUnit();
    }

    /** @return The list of values the user can select a value from if the {@link DataType} is an enumeration. */
    public List<String> getEnumSelections() {
        final BuiltInDataType propertyDataType =
                Conversion.getBuiltInDataType(propertyValue.getProperty().getDataType());
        if (propertyDataType.equals(BuiltInDataType.USER_DEFINED_ENUM)
                || propertyDataType.equals(BuiltInDataType.BOOLEAN)) {
            // if it is an enumeration, get the list of its options from the data type definition field
            return Conversion.prepareEnumSelections(propertyValue.getProperty().getDataType());
        }
        return null;
    }

    /** @return The type of the UI control to use depending on the {@link PropertyValue} {@link DataType} */
    public PropertyValueUIElement getPropertyValueUIElement() {
        return propertyValue.getProperty() != null ?
                Conversion.getUIElementFromProperty(propertyValue.getProperty()) :
                PropertyValueUIElement.NONE;
    }

    /**
     * @return the propertyValue
     */
    @Override
    public PropertyValue getEntity() {
        return propertyValue;
    }

    /** Called by the UI input control to set the value.
     * @param property The property
     */
    public void setProperty(Property property) {
        propertyValue.setProperty(property);
    }
    /** @return The property associated with the property value */
    public Property getProperty() {
        return propertyValue.getProperty();
    }

    /** The method called to convert user input into {@link Value} when the user presses "Save" button in the dialog.
     * Called by the UI input control to set the value.
     *
     * @param propertyValue String representation of the property value.
     */
    public void setPropertyValue(String propertyValue) {
        if (isAlias()) {
            // If property is alias do not change value
            return;
        }
        DataType dataType = getType();
        if (dataType != null) {
            this.propertyValue.setPropValue(Conversion.stringToValue(propertyValue, dataType));
        }
    }
    /** @return String representation of the property value. */
    public String getPropertyValue() {
        SlotPropertyValue aliasedProperty = getAliasedProperty();
        if (aliasedProperty != null) {
            Slot referenceToSlot = aliasedProperty.getReferenceToSlot();
            if (referenceToSlot != null) {
                // If property is alias of a slot reference return aliased referred slot name
                return referenceToSlot.getName();
            }
            // If property is alias return alias value
            return Conversion.valueToString(aliasedProperty.getPropValue());
        }

        //if property is a SlotValueProperty, and is a SlotReference, return the referred Slot name
        if((propertyValue instanceof SlotPropertyValue) && propertyValue.getProperty() != null &&
                (BooleanUtils.toBoolean(propertyValue.getProperty().getSlotReference()))) {
            Slot referenceToSlot = ((SlotPropertyValue) propertyValue).getReferenceToSlot();
            if(referenceToSlot == null) {
                return null;
            } else {
                return referenceToSlot.getName();
            }
        }

        return Conversion.valueToString(propertyValue.getPropValue());
    }

    /** The method called to convert user date input into {@link Value} when the user presses "Save" button in the
     * dialog. Should be used only with timestamp properties. Called by the UI input control to set the value.
     * @param propertyDate Date representation of the property value.
     */
    public void setPropertyDate(Date propertyDate) {
        assert(BuiltInDataType.TIMESTAMP.equals(getType().getName()));
        if (isAlias()) {
            // If property is alias do not change value
            return;
        }

        this.propertyValue.setPropValue(Conversion.dateToValue(propertyDate));
    }

    /** @return Date representation of the property value, or current date if null.
     */
    public Date getPropertyDate() {
        assert(BuiltInDataType.TIMESTAMP.toString().equals(getType().getName()));
        try {
            SlotPropertyValue aliasedProperty = getAliasedProperty();
            if (aliasedProperty != null) {
                // If property is alias return alias value
                return Conversion.valueToDate((TimestampValue)aliasedProperty.getPropValue());
            }
            return Conversion.valueToDate((TimestampValue)propertyValue.getPropValue());
        } catch (NullPointerException | ConversionException e) {
            // If no date set, choose now
            return Conversion.getNow();
        }
    }

    private boolean isAlias() {
        return getAliasedProperty() != null;
    }

    private SlotPropertyValue getAliasedProperty() {
        if (propertyValue instanceof SlotPropertyValue) {
            SlotPropertyValue aliasOf = ((SlotPropertyValue) propertyValue).getAliasOf();
            if (aliasOf != null) {
                return aliasOf;
            }
        }
        return null;
    }

    /** The validator for the UI input field when UI control accepts a double precision number, and integer number or a
     * string for input.
     * Called when saving {@link PropertyValue}
     * @param ctx {@link javax.faces.context.FacesContext}
     * @param component {@link javax.faces.component.UIComponent}
     * @param value The value
     * @throws ValidatorException {@link javax.faces.validator.ValidatorException}
     */
    public void inputValidator(FacesContext ctx, UIComponent component, Object value) {
        if (value == null) {
            throw UiUtility.getValidationError("No value to parse.");
        }

        if (propertyValue.getProperty() == null) {
            throw UiUtility.getValidationError("You must select a property first.");
        }

        if (value instanceof Date) {
            value = Conversion.dateToString((Date)value);
        }

        final DataType dataType = propertyValue.getProperty().getDataType();
        final String regexp = propertyValue.getProperty().getRegexp();
        validateSingleLine(value.toString(), dataType, regexp);
    }

    /** Validates the {@link PropertyValue} value in a dialog for properties that accept a single value
     * @param strValue the value from the dialog
     * @param dataType the expected {@link DataType} of the value
     * @param regexp the expected {@link PropertyValue} regular expression the value has to match
     */
    public static void validateSingleLine(final String strValue, final DataType dataType, final String regexp) {
        if (strValue != null) {
            final String trimStrValue = strValue.trim();
            switch (Conversion.getBuiltInDataType(dataType)) {
                case DOUBLE:
                    try {
                        Double.parseDouble(trimStrValue);
                    } catch (NumberFormatException e) {
                        throw UiUtility.getValidationError("Not a double value.");
                    }
                    validateValueAgainstRegexp(trimStrValue, regexp);
                    break;
                case INTEGER:
                    try {
                        Integer.parseInt(trimStrValue);
                    } catch (NumberFormatException e) {
                        throw UiUtility.getValidationError("Not an integer number.");
                    }
                    validateValueAgainstRegexp(trimStrValue, regexp);
                    break;
                case STRING:
                    validateValueAgainstRegexp(trimStrValue, regexp);
                    break;
                case CSENTRY_HOST:
                    /*
                    validateValueAgainstRegexp(trimStrValue, regexp);
                    */
                    break;
                case TIMESTAMP:
                    try {
                        Conversion.toTimestamp(strValue);
                    } catch (RuntimeException e) {
                        throw UiUtility.getValidationError(e.getMessage(), e);
                    }
                    validateValueAgainstRegexp(strValue, regexp);
                    break;
                default:
                    throw UiUtility.getValidationError("Incorrect property data type.");
            }
        }
    }

    /** The validator for the UI input area when the UI control accepts a matrix of double precision numbers or a list
     * of values for input.
     * Called when saving {@link PropertyValue}
     * @param ctx {@link javax.faces.context.FacesContext}
     * @param component {@link javax.faces.component.UIComponent}
     * @param value The value
     * @throws ValidatorException {@link javax.faces.validator.ValidatorException}
     */
    public void areaValidator(FacesContext ctx, UIComponent component, Object value) {
        if (value == null) {
            throw UiUtility.getValidationError("No value to parse.");
        }
        if (propertyValue.getProperty() == null) {
            throw UiUtility.getValidationError("You must select a property first.");
        }

        final DataType dataType = propertyValue.getProperty().getDataType();
        final String regexp = propertyValue.getProperty().getRegexp();
        validateMultiLine(value.toString(), dataType, regexp);
    }

    /** Validates the {@link PropertyValue} value in a dialog for properties that accept multiple values
     * (vectors, lists, tables)
     * @param strValue the value from the dialog
     * @param dataType the expected {@link DataType} of the value
     * @param regexp the expected {@link PropertyValue} regular expression the value has to match
     */
    public static void validateMultiLine(final String strValue, final DataType dataType, final String regexp) {
        switch (Conversion.getBuiltInDataType(dataType)) {
            case DBL_TABLE:
                validateTable(strValue, regexp);
                break;
            case DBL_VECTOR:
                validateDblVector(strValue, regexp);
                break;
            case INT_VECTOR:
                validateIntVector(strValue, regexp);
                break;
            case BOOLEAN_VECTOR:
                validateBoolVector(strValue, regexp);
                break;
            case STRING_LIST:
                validateStringVector(strValue, regexp);
                break;
            default:
                throw UiUtility.getValidationError("Incorrect property data type.");
        }
    }

    private static void validateTable(final String value, final String regexp) {
        try (Scanner lineScanner = new Scanner(value)) {
            lineScanner.useDelimiter(Pattern.compile(MULTILINE_DELIMITER));

            int lineLength = -1;
            while (lineScanner.hasNext()) {
                // replace unicode whitespaces with normal ones
                final String line = CharMatcher.whitespace().collapseFrom(lineScanner.next(), ' ');

                try (Scanner valueScanner = new Scanner(line)) {
                    valueScanner.useDelimiter(",\\s*");
                    int currentLineLength = 0;
                    while (valueScanner.hasNext()) {
                        final String dblValue = valueScanner.next().trim();
                        currentLineLength++;
                        try {
                            Double.valueOf(dblValue);
                        } catch (NumberFormatException e) {
                            throw UiUtility.getValidationError(INCORRECT_VALUE + dblValue);
                        }
                        validateValueAgainstRegexp(dblValue, regexp);
                    }
                    if (lineLength < 0) {
                        lineLength = currentLineLength;
                    } else if (currentLineLength != lineLength) {
                        throw UiUtility.getValidationError("All rows must contain the same number of elements.");
                    }
                }
            }
        }
    }

    private static void validateIntVector(final String value, final String regexp) {
        try (Scanner scanner = new Scanner(value)) {
            scanner.useDelimiter(Pattern.compile(MULTILINE_DELIMITER));

            while (scanner.hasNext()) {
                String intValue = "<error>";
                try {
                    // trim unicode whitespaces
                    intValue = CharMatcher.whitespace().trimFrom(scanner.next());
                    Integer.parseInt(intValue);
                } catch (NumberFormatException e) {
                    throw UiUtility.getValidationError(INCORRECT_VALUE + intValue);
                }
                validateValueAgainstRegexp(intValue, regexp);
            }
        }
    }

    private static void validateDblVector(final String value, final String regexp) {
        try (Scanner scanner = new Scanner(value)) {
            scanner.useDelimiter(Pattern.compile(MULTILINE_DELIMITER));

            while (scanner.hasNext()) {
                String dblValue = "<error>";
                try {
                    // trim unicode whitespaces
                    dblValue = CharMatcher.whitespace().trimFrom(scanner.next());
                    Double.parseDouble(dblValue);
                } catch (NumberFormatException e) {
                    throw UiUtility.getValidationError(INCORRECT_VALUE + dblValue);
                }
                validateValueAgainstRegexp(dblValue, regexp);
            }
        }
    }

    private static void validateBoolVector(final String value, final String regexp) {
        try (Scanner scanner = new Scanner(value)) {
            scanner.useDelimiter(Pattern.compile(MULTILINE_DELIMITER));

            while (scanner.hasNext()) {
                // trim unicode whitespaces
                final String boolValue = CharMatcher.whitespace().trimFrom(scanner.next()).toUpperCase();
                if (!Conversion.BOOLEAN_TEXT_TRUE.equals(boolValue)
                        && !Conversion.BOOLEAN_TEXT_FALSE.equals(boolValue)) {
                    throw UiUtility.getValidationError(INCORRECT_VALUE + boolValue);
                }
                validateValueAgainstRegexp(boolValue, regexp);
            }
        }
    }

    private static void validateStringVector(final String value, final String regexp) {
        try (Scanner scanner = new Scanner(value)) {
            scanner.useDelimiter(Pattern.compile(MULTILINE_DELIMITER));

            while (scanner.hasNext()) {
                String stringValue = CharMatcher.whitespace().trimFrom(scanner.next());
                validateValueAgainstRegexp(stringValue, regexp);
            }
        }
    }

    private static void validateValueAgainstRegexp(final String value, final String regexp) {
        if (regexp != null && !regexp.isEmpty() && !Pattern.matches(regexp, value)) {
            throw UiUtility.getValidationError("Value '" + value + "' does not match regular expression '"
                + regexp + "'");
        }
    }

    /** @return the propertyNameChangeDisabled */
    public boolean isPropertyNameChangeDisabled() {
        return propertyNameChangeDisabled;
    }

    /** @param propertyNameChangeDisabled the propertyNameChangeDisabled to set */
    public void setPropertyNameChangeDisabled(boolean propertyNameChangeDisabled) {
        this.propertyNameChangeDisabled = propertyNameChangeDisabled;
    }

    /** @return the beingAdded */
    public boolean isBeingAdded() {
        return propertyValue.getId() == null;
    }

    private <P extends ConfigurationEntity> EntityAttributeKind getEntityKind(P entity) {
        return EntityAttributeKind.getEntityKind(entity, propertyValue);
    }

    /**
     * Refreshes property value
     *
     * @param propertyValue new property value
     */
    public void refreshEntity(final PropertyValue propertyValue) {
        this.propertyValue = propertyValue;
    }
}
