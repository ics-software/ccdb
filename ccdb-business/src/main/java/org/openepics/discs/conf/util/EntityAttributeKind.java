/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.openepics.discs.conf.util;

import com.google.common.base.Preconditions;
import org.openepics.discs.conf.ent.ComponentType;
import org.openepics.discs.conf.ent.ComptypePropertyValue;
import org.openepics.discs.conf.ent.ConfigurationEntity;
import org.openepics.discs.conf.ent.Device;
import org.openepics.discs.conf.ent.PropertyValue;
import org.openepics.discs.conf.ent.Slot;

/**
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
public enum EntityAttributeKind {

    DEVICE_TYPE_PROPERTY("Device type property"),
    DEVICE_TYPE_ARTIFACT("Device type artifact"),
    DEVICE_TYPE_EXTERNAL_LINK("Device type external link"),
    DEVICE_TYPE_TAG("Device type tag"),
    SLOT_PROPERTY("Slot property"),
    SLOT_ARTIFACT("Slot artifact"),
    SLOT_EXTERNAL_LINK("Slot external link"),
    SLOT_TAG("Slot tag"),
    CONTAINER_ARTIFACT("Container artifact"),
    CONTAINER_EXTERNAL_LINK("Container external link"),
    CONTAINER_TAG("Container tag"),
    UNKNOWN_PROPERTY("Unknown type property"),
    // Should be removed after Device and Device property will be taken out completely
    DEVICE_PROPERTY("Device property"),
    // Should be removed after Device and Device property will be taken out completely
    DEVICE_ARTIFACT("Device artifact"),
    // Should be removed after Device and Device property will be taken out completely
    DEVICE_TAG("Device tag"),
    // Should be removed after Device and Device property will be taken out completely
    DEVICE_EXTERNAL_LINK("Slot external link");

    private final String text;

    /**
     * @param text
     */
    EntityAttributeKind(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }

    public static <P extends ConfigurationEntity> EntityAttributeKind getEntityKind(P entity,
                                                                                    final PropertyValue propertyValue) {
        Preconditions.checkState(propertyValue != null);

        if (entity instanceof ComponentType) {
            final ComptypePropertyValue comptypePropertyValue = (ComptypePropertyValue) propertyValue;
            if (!comptypePropertyValue.isPropertyDefinition()) {
                return EntityAttributeKind.DEVICE_TYPE_PROPERTY;
            } else if (comptypePropertyValue.isDefinitionTargetSlot()) {
                return EntityAttributeKind.SLOT_PROPERTY;
            } else {
                return EntityAttributeKind.DEVICE_PROPERTY;
            }
        }
        if (entity instanceof Slot) {
            if (((Slot) entity).isHostingSlot()) {
                return EntityAttributeKind.SLOT_PROPERTY;
            }
        }
        if (entity instanceof Device)
            return EntityAttributeKind.DEVICE_PROPERTY;
        throw new UnhandledCaseException();
    }
}
