/*
 * Copyright (c) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.util.csentry;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.openepics.cable.client.impl.ClosableResponse;
import org.openepics.cable.client.impl.ResponseException;

import com.google.common.collect.Lists;

import javax.annotation.Nonnull;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Client that can connect to the CSEntry and retrieve all hostnames.
 *
 * @author <a href="mailto:marcel.salmic@cosylab.com">Marcel Salmic</a>
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class CSEntryClient {

	/**
	 * CS ENTRY INTEGRATION DISABLED AS CS ENTRY NO LONGER EXISTS.
	 * DISABLE AND / OR RETURN FALSE / EMPTY VALUE(S).
	 */

    private static final Logger LOGGER = Logger.getLogger(CSEntryClient.class.getName());

    /**
     * Represents CSEntry host name check result
     */
    public static class HostCheckResult {
        // host name is invalid
        private final boolean invalid;
        // suggestion for FQDN if exists
        private final String suggestedFQDN;

        public HostCheckResult(boolean invalid, String suggestedFQDN) {
            /*
            this.invalid = invalid;
            */
            this.invalid = false;
            this.suggestedFQDN = suggestedFQDN;
        }

        public boolean isInvalid() {
            return invalid;
        }

        public String getSuggestedFQDN() {
            return suggestedFQDN;
        }
    }

    @Nonnull
    private final Client client;
    private final String url;
    private final String token;
    private static final String FQDN = "fqdn:";
    private static final String QUERY_DELIMITER = "-";
    private static final Supplier<Stream<String>> FIELD_SELECTORS
            = () -> Stream.of(FQDN);

    // Field selectors when we should also search for interface names and cnames:
    // private static final Supplier<Stream<String>> FIELD_SELECTORS
    //          = () -> Stream.of(FQDN, "interfaces.name:", "interfaces.cnames:");

    /**
     * Constructor.
     * @param url URL of the CSEntry
     */
    public CSEntryClient(String url, String token) {
        this.url = url;
        this.token = token;
        /*
        client = new ResteasyClientBuilder()
                .establishConnectionTimeout(1000, TimeUnit.MILLISECONDS)
                .socketTimeout(1000, TimeUnit.MILLISECONDS)
                .build();
        */
        client = null;
    }

    /**
     * Checks if hostname belongs to an entry in CSEntry service.
     *
     * @param hostname hostname as string to check
     * @return <code>false<code/> if hostname belongs to an entry otherwise <code>false<code/>
     */
    public boolean isHostnameInvalid(final String hostname) {
        return checkHostname(hostname).isInvalid();
    }

    /**
     * Checks if host name belongs to an entry in CSEntry service. If not, gives automatic suggestion if possible.
     *
     * @param hostname host name as string to check
     * @return {@link org.openepics.discs.conf.util.csentry.CSEntryClient.HostCheckResult} result
     */
    public HostCheckResult checkHostname(final String hostname) {
        if (StringUtils.isEmpty(hostname)) {
            return new HostCheckResult(true, null);
        }

        final String[] hostnameParts = hostname.trim().split("\\.");
        List<String> suggestedFQDNs = Collections.emptyList();
        boolean invalid = false;
        try {
            List<String> foundFQDNs = searchHost(hostnameParts[0], 10);
            invalid = foundFQDNs.stream().noneMatch(hostname::equals);
            if (invalid && hostnameParts.length == 1) {
                suggestedFQDNs = foundFQDNs.stream().filter(fqdn -> {
                    final String[] fqdnParts = fqdn.split("\\.");
                    return StringUtils.equals(fqdnParts[0], hostnameParts[0]);
                }).collect(Collectors.toList());
            }
        } catch (ResponseException e) {
            LOGGER.log(Level.WARNING, "Failed to call CSEntry API (" + e.getMessage() + ") for \"" + hostname + "\"");
        }
        return new HostCheckResult(invalid, suggestedFQDNs.size() == 1 ? suggestedFQDNs.get(0) : null);
    }

    /**
     * Search hosts from CSEntry items.
     * @return parsed data.
     */
    public List<String> searchHost(final String query, final int limit) {
        /*
        String queryString;
        final String[] queryParts = query.split(QUERY_DELIMITER);
        if(queryParts.length == 1) {
            queryString = FIELD_SELECTORS.get()
                    .map(field -> query.endsWith(QUERY_DELIMITER) ? field + query : field + query + "*")
                    .collect(Collectors.joining(" "));
        } else {
            final String[] parts = ArrayUtils.clone(queryParts);
            if (!query.endsWith(QUERY_DELIMITER)) {
                parts[parts.length - 1] += "*";
            }

            queryString = FIELD_SELECTORS.get().map(field ->
                    Arrays.stream(parts).map(part -> "+" + field + part).collect(Collectors.joining(" "))
            ).map(queryPart -> "(" + queryPart + ")").collect(Collectors.joining(" OR "));
        }

        final List<CSEntryHost> csEntryHosts = search(queryString, limit);

        return csEntryHosts.stream().map(host -> {
            List<String> results = new ArrayList<>();
            if (propertyContainsQueryParts(host.fqdn, queryParts)) {
                results.add(host.fqdn);
            }

			// Result list when we should also search for interface names and cnames:
			//            if (host.interfaces != null) {
			//                for (CSEntryInterface hostInterface : host.interfaces) {
			//                    if (propertyContainsQueryParts(hostInterface.name, queryParts)) {
			//                        results.add(hostInterface.name + "." + hostInterface.domain);
			//                    }
			//                    if (hostInterface.cnames != null) {
			//                        for (String cname : hostInterface.cnames) {
			//                            if (propertyContainsQueryParts(cname, queryParts)) {
			//                                results.add(cname + "." + hostInterface.domain);
			//                            }
			//                        }
			//                    }
			//                }
			//            }
            return results;
        }).flatMap(Collection::stream).distinct().limit(limit).collect(Collectors.toList());
        */
        return Lists.newArrayList();
    }

    /*
    private boolean propertyContainsQueryParts(final String property, final String[] queryParts) {
        return Stream.of(queryParts).allMatch(part -> StringUtils.containsIgnoreCase(property, part));
    }

    private List<CSEntryHost> search(final String queryString, final int limit) {
        final String baseUrl = url + "/network/hosts/search";
        UriBuilder ub = UriBuilder.fromUri(baseUrl);
        ub.queryParam("q", queryString);
        ub.queryParam("per_page", limit);

        LOGGER.finest("searchHost query URL: " + ub.build().toString());
        final String authorizationHeader = "Bearer " + token;

        try (final ClosableResponse response = new ClosableResponse(
                client.target(ub)
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .header("Authorization", authorizationHeader)
                        .get())) {

            return response.readEntity(new GenericType<List<CSEntryHost>>() {});
        } catch (Exception e) {
            throw new ResponseException("Couldn't retrieve data from service at " + url + ".", e);
        }
    }
    */

}
