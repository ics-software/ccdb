/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.google.common.collect.ImmutableMap;
import org.openepics.discs.conf.ent.Tag;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import org.openepics.discs.conf.util.AutoCompleteQuery;

/**
 * Service that is used to query tags in the database
 *
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 *
 */
@Stateless
public class TagEJB {
    @PersistenceContext private EntityManager em;

    private final AutoCompleteDatabaseQuery<Tag> tagAutoCompDatabaseQuery
            = new AutoCompleteDatabaseQuery<Tag>() {
        @Override
        public String getQueryNameOfFindStarting() {
            return "Tag.findByNamePartStarting";
        }

        @Override
        public String getQueryNameOfFindContaining() {
            return "Tag.findByNamePartContaining";
        }

        @Override
        public Class<Tag> getAutoCompleteEntityClass() {
            return Tag.class;
        }

        @Override
        public EntityManager getEntityManager() {
            return em;
        }
    };

    /**
     * Finds the {@link Tag} which has the specific name matching to query.
     *
     * @return A list of all {@link Tag}s in the database ordered by creation date
     */
    public List<Tag> findByNamePart(final String query, final int limit) {
        return tagAutoCompDatabaseQuery.autoCompleteResult(
                ImmutableMap.of(AutoCompleteQuery.QUERY_PARAM, query), limit);
    }

    /**
     * @param tag the {@link Tag} (as a String) to search for
     * @return a {@link Tag} entity if it is already defined, <code>null</code> otherwise
     */
    public Tag findById(String tag) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(tag));
        return em.find(Tag.class, tag);
    }

}
