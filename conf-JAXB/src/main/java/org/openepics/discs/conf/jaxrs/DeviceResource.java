/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.jaxrs;

import io.swagger.annotations.*;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * This resource provides bulk and specific device data.
 *
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
@Path("devices")
@Api(value = "/devices")
@SwaggerDefinition(
        info = @Info(
                description = "\n"
                        + "    This is a documentation for all REST interfaces of Controls Configuration DB service. \n"
                        + "    You can find out more about Controls Configuration DB service in CCDB application,    \n"
                        + "        https://ccdb.esss.lu.se",
                version = "1.0.2",
                title = "CCDB service API documentation",
                termsOfService = "http://swagger.io/terms/",
                contact = @Contact(
                        name = "Support",
                        email = "Icsscontrolsystemsupport@esss.se")
                ),
        externalDocs = @ExternalDocs(
                value = "Controls Configuration Database",
                url = "https://confluence.esss.lu.se/display/SW/Controls+Configuration+Database"))
@Produces({"application/json"})
public interface DeviceResource {

    /** @return returns all devices in the database. */
    @GET
    @ApiOperation(value = "Lists all devices from DB",
            notes = "Returns all devices from DB",
            response = Response.class)
    @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    public Response getAllDevices();

    /**
     * Returns a specific device based on the id provided.
     *
     * @param id
     *            the id of the device to retrieve
     * @return the device instance data
     */
    @GET
    @Path("id/{id}")
    @ApiOperation(value = "Finds a specific device in DB with desired id",
            notes = "Returns a specific device from DB with desired id",
            response = Response.class)
    @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    public Response getDeviceById(@PathParam("id") Long id);

    /**
     * Returns a specific device based on the name provided.
     *
     * @param name
     *            the name of the device to retrieve
     * @return the device instance data
     */
    @GET
    @Path("{name}")
    @ApiOperation(value = "Finds a specific device in DB with desired name",
            notes = "Returns a specific device from DB with desired name",
            response = Response.class)
    @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    public Response getDeviceByName(@PathParam("name") String name);

    /**
     * Returns a specific device artifact file based on the id provided.
     *
     * @param id
     *            the id of the device from which to retrieve artifact file.
     * @param fileName
     *            the name of the artifact file to retrieve.
     * @return the device artifact file
     */
    @GET
    @Path("id/{id}/download/{fileName}")
    @ApiOperation(value = "Finds a specific device in DB with desired id, and returns with a file",
            notes = "Returns a file containing a device from DB with desired id",
            response = Response.class)
    @Produces({ MediaType.MEDIA_TYPE_WILDCARD })
    public Response getAttachmentById(@PathParam("id") Long id, @PathParam("fileName") String fileName);

    /**
     * Returns a specific device artifact file based on the name provided.
     *
     * @param name
     *            the name of the device from which to retrieve artifact file.
     * @param fileName
     *            the name of the artifact file to retrieve.
     * @return the device artifact file
     */
    @GET
    @Path("{name}/download/{fileName}")
    @ApiOperation(value = "Finds a specific device in DB with desired name, and returns with a file",
            notes = "Returns a file containing a specific device from DB with desired name",
            response = Response.class)
    @Produces({ MediaType.MEDIA_TYPE_WILDCARD })
    public Response getAttachmentByName(@PathParam("name") String name, @PathParam("fileName") String fileName);
}
