/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.jaxrs.client;

import java.io.InputStream;
import java.util.List;

import javax.annotation.Nullable;

import org.openepics.discs.conf.jaxb.DeviceTypeXml;
import org.openepics.discs.conf.jaxb.SlotXml;
import org.openepics.discs.conf.jaxb.SlotNameXml;


/**
 * This interface provides methods for clients to access {@link SlotXml} specific data.
 *
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
public interface InstallationSlotClient {
    /** @return all {@link SlotXml}s */
    public List<SlotXml> getInstallationSlots();

    /** @return a subset of {@link SlotXml}s based on the device type name
     * @param deviceType The device type name.
     */
    public List<SlotXml> getInstallationSlots(String deviceType);

    /**
     * Returns a specific installation slot based on the name.
     *
     * @param name
     *            the name of the installation slot to retrieve
     * @return the installation slot instance data
     */
    public SlotXml getInstallationSlotByName(String name);

    /**
     * Returns a specific installation slot based on the name id.
     *
     * @param nameId
     *            the name id of the installation slot to retrieve
     * @return the installation slot instance data
     */
    public SlotXml getInstallationSlotByNameId(String nameId);

    /**
     * Returns a specific installation slot artifact file.
     *
     * @param name
     *            the name of the installation slot from which to retrieve
     *            artifact file.
     * @param fileName
     *            the name of the artifact file to retrieve.
     * @return the installation slot artifact file
     */
    public InputStream getAttachment(String name, String fileName);

    /**
     * @return a {@link List} of all {@link SlotNameXml}s.
     */
    public List<SlotNameXml> getAllInstallationSlotNames();

    /**
     * @param deviceTypeName the name of the {@link DeviceTypeXml} to return information for.
     *
     * @return a {@link List} of {@link SlotNameXml}s that correspond to a requested {@link DeviceTypeXml}.
     */
    public List<SlotNameXml> getAllInstallationSlotNames(String deviceTypeName);

    /**
     * @param name the name of the installation slot for which to return 'Controls' children
     * @param transitive <code>true</code> - check descendants of children as well, <code>false</code> - children only
     * @param properties a {@link List} of properties that the 'Controls' descendants must contain
     * @return a {@link List} of all {@link SlotXml}s that match the above criteria
     */
    public List<SlotXml> getControlsChildrenByName(String name, boolean transitive, @Nullable List<String> properties);

    /**
     * @param nameId the name id of the installation slot for which to return 'Controls' children
     * @param transitive <code>true</code> - check descendants of children as well, <code>false</code> - children only
     * @param properties a {@link List} of properties that the 'Controls' descendants must contain
     * @return a {@link List} of all {@link SlotXml}s that match the above criteria
     */
    public List<SlotXml> getControlsChildrenByNameId(
            String nameId, boolean transitive, @Nullable List<String> properties);
}
