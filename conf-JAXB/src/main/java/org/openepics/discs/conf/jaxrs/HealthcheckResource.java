/*
 * Copyright (c) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.discs.conf.jaxrs;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * This resource provides healthcheck of CCDB application.
 *
 * @author Lars Johansson
 */
@Path("healthcheck")
@Api(value = "/healthcheck")
public interface HealthcheckResource {

    /**
     * Returns healthcheck of CCDB application as string.
     *
     * @return healthcheck as string
     */
    @GET
    @ApiOperation(value = "Heathcheck of CCDB application",
            notes = "Returns healthcheck as string of CCDB application",
            response = String.class)
    @Produces({MediaType.TEXT_PLAIN, MediaType.TEXT_PLAIN})
    public String getHealthcheck();

}
