/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.webservice;

import java.io.ByteArrayInputStream;
import java.util.List;

import javax.activation.MimetypesFileTypeMap;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import org.openepics.discs.conf.ent.Artifact;

import org.openepics.discs.conf.ent.ComponentType;
import org.openepics.discs.conf.ent.Device;
import org.openepics.discs.conf.ent.InstallationRecord;
import org.openepics.discs.conf.ent.Slot;

/**
 * This is a package private class the implements the download attachment common code.
 *
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
class GetAttachmentResourceBase {
    public static enum AttachmentOwnerIdentifierType {
        ID, NAME_ID, NAME;
    }
    
    public static class AttachmentOwnerIdentifier {
        private AttachmentOwnerIdentifierType type;
        private String value;
        
        public AttachmentOwnerIdentifier(AttachmentOwnerIdentifierType type, String value) {
            this.type = type;
            this.value = value;
        }

        public AttachmentOwnerIdentifier(AttachmentOwnerIdentifierType type, Long value) {
            this(type, value.toString());
        }
        
        public String toString() {
            switch (type) {
                case ID:
                    return "id:" + value;
                case NAME_ID:
                    return "nameId:" + value;
                case NAME:
                    return "name:" + value;
                default:
                    throw new Error("Undefined attachment identifier type.");
            }
        }
    }

    /* Only static methods */
    private GetAttachmentResourceBase() {
    }

    static Response getFileForDeviceType(ComponentType componentType, final AttachmentOwnerIdentifier ownerIdentifier,
            final String fileName) {
        byte[] content = findArtifactContent(componentType.getEntityArtifactList(), fileName);
        return getFile(content, ownerIdentifier, fileName);
    }

    static Response getFileForDevice(final Device device,
            final InstallationRecord installationRecord, final AttachmentOwnerIdentifier ownerIdentifier, 
            final String fileName) {
        byte[] content = findArtifactContent(device.getEntityArtifactList(), fileName);

        if ((content == null) && (installationRecord != null)) {
            content = findArtifactContent(installationRecord.getSlot().getEntityArtifactList(), fileName);
        }
        if (content == null) {
            content = findArtifactContent(device.getComponentType().getEntityArtifactList(), fileName);
        }
        return getFile(content, ownerIdentifier, fileName);
    }

    static Response getFileForSlot(final Slot slot, final InstallationRecord installationRecord,
            final AttachmentOwnerIdentifier ownerIdentifier, final String fileName) {
        Device device = null;
        byte[] content = findArtifactContent(slot.getEntityArtifactList(), fileName);

        if ((content == null) && (installationRecord != null)) {
            device = installationRecord.getDevice();
            content = findArtifactContent(device.getEntityArtifactList(), fileName);
        }
        if (content == null) {
            content = findArtifactContent(slot.getComponentType().getEntityArtifactList(), fileName);
        }
        return getFile(content, ownerIdentifier, fileName);
    }

    private static Response getFile(final byte[] content, final AttachmentOwnerIdentifier ownerIdentifier, 
            final String fileName) {
        ResponseBuilder response;
        
        if (content == null) {
            response = Response.status(Status.NOT_FOUND);
            response.type(MediaType.TEXT_HTML);
            response.entity("Attachment with name " + fileName + " not found on " + ownerIdentifier + ".");
        } else {
            response = Response.ok(new ByteArrayInputStream(content));
            MimetypesFileTypeMap mimeTypesFileTypeMap = new MimetypesFileTypeMap();
            response.type(mimeTypesFileTypeMap.getContentType(fileName));
            response.header("Content-Disposition", "attachment; filename = \"" + fileName + "\"");
        }
      
        return response.build();
    }
    
    private static byte[] findArtifactContent(final List<Artifact> artifacts, final String fileName){
        for(Artifact artifact : artifacts){
            if(artifact.getName().equals(fileName)){
                return artifact.getBinaryData().getContent();
            }
        }
        
        return null;
    }
}
